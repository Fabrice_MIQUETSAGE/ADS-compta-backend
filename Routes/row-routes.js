const express = require('express')
const dbModels = require('../models/row-models');

const router = express.Router()

router.delete('/:id', (req,res) => {
    const {id} = req.params
    dbModels.removeRow(id)
    .then(count => {
        if(count>0) {
            res.status(200).json({message: `row avec l'id ${id} supprimée` })
        }
        else {
            res.status(404).json({message:"aucune row trouvée avec cet id"})
        }
    })
    .catch(error => {
        res.status(500).json({message: "impossible d'effectuer l'opération", error})
    })

})

module.exports = router