const express = require('express')
const dbModelsDocType = require('../models/docType-models');


const router = express.Router()

router.post('/', (req, res) => {
    dbModelsDocType.addDocType(req.body)
        .then(docType => {
            res.status(200).json(docType)
        })
        .catch(error => {
            res.status(500).json({ message: "impossible d'effectuer l'opération", error })
        })
})

router.get('/', (req, res) => {
    dbModelsDocType.findAllDocType()
        .then(docType => {
            res.status(200).json(docType)
        })
        .catch(error => {
            res.status(500).json({message: "impossible d'effectuer l'opération", error})
        })
})

router.get('/:id', (req, res) => {
    const { id } = req.params;
    dbModelsDocType.findDocTypeById(id)
        .then(docType => {
            if (docType) {
                res.status(200).json(docType)
            }
            else {
                res.status(404).json({ message: "cet id n'a pas été trouvé dans la base doctype" })
            }
        })
        .catch(error => {
            res.status(500).json({message: "impossible d'effectuer l'opération", error})
        })
})

router.delete('/:id', (req,res) => {
    const {id} = req.params;
    dbModelsDocType.removeDocType(id)
    .then(count => {
        if (count>0) [
            res.status(200).json({message: "suppression effectuée"})
        ]
        else {
            res.status(404).json({message: "aucun client toruvé avec cet id"})
        }
    })
        .catch(error => {
            res.status(500).json({message: "impossible d'effectuer l'opération", error})
        })
})

module.exports = router
