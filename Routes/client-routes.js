const express = require('express')
const dbModelsClient= require('../models/client-models')
const router = express.Router()

router.post('/', (req,res) => {
    dbModelsClient.addClient(req.body)
    .then(client =>{
        res.status(200).json(client)
    })
    .catch(error => {
        res.status(500).json({message: "impossible d'effectuer l'opération", error})
    })
})

router.get('/', (req,res) => {
    dbModelsClient.findAllClient()
    .then(client=> {
        res.status(200).json(client)
    })
    .catch(error => {
        res.status(500).json({message: "impossible d'effectuer l'opération"})
    })
})

router.get('/:id', (req,res)=> {
    const {id} = req.params;
    dbModelsClient.findClientById(id)
    .then(client=> {
        if(client) {
            res.status(200).json(client)
        }
        else{
            res.status(404).json({message:"aucun client avec cet id"})
        }
    })
    .catch(error => {
        res.status(500).json({message: "impossible d'effectuer l'opération", error})
    })
})


router.delete('/:id', (req,res) => {
    const {id} = req.params;
    dbModelsClient.removeClient(id)
    .then(count => {
        if (count>0) [
            res.status(200).json({message: "suppression effectuée"})
        ]
        else {
            res.status(404).json({message: "aucun client toruvé avec cet id"})
        }
    })
        .catch(error => {
            res.status(500).json({message: "impossible d'effectuer l'opération", error})
        })
})

router.patch('/:id', (req,res) => {
    const {id} = req.params; 
    const changes = req.body
    dbModelsClient.updateClient(id,changes)
    .then(client=> {
        if(client){
            res.status(200).json(client)
        }
        else{
            res.status(404).json({ message: "aucun client avec cet id" })
        }
    })
    .catch(error => {
        res.status(500).json({message: "impossible d'effectuer l'opération", error})
    })
})

module.exports = router