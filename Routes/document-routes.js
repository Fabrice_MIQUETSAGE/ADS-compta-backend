const express = require('express')
const dbModelsDocument = require('../models/document-models');
const dbModelsRow = require('../models/row-models');

const router = express.Router()

//add document
router.post('/', (req, res) => {
    dbModelsDocument.addDocument(req.body)
        .then(document => {
            res.status(200).json(document)
        })
        .catch(error => {
            res.status(500).json({ message: "impossible d'effectuer l'opération", error })
        })
})

//get all documents
router.get('/', (req, res) => {
    dbModelsDocument.findAllDocument()
        .then(document => {
            res.status(200).json(document)
        })
        .catch(error => {
            res.status(500).json({ message: "impossible d'effectuer l'opération", error })
        })
})

//get document by ID
router.get('/:id', (req, res) => {
    const { id } = req.params
    dbModelsDocument.findDocumentById(id)
        .then(document => {
            if (document) {
                res.status(200).json(document)
            }
            else {
                res.status(404).json({ message: "cet id n'a pas été trouvé dans la base document" })
            }
        })
        .catch(error => {
            res.status(500).json({ message: "impossible d'effectuer l'opération", error })
        })
})

//post Row 
router.post('/:id/row', (req, res) => {
    const { id } = req.params;
    const row = req.body;
    console.log(res)
    //if the req body do not have the ref to document ID, let fill it
    if (!row.document_id) {
        row["document_id"] = parseInt(id, 10)
    }
    // check the validity of document_id
    dbModelsDocument.findDocumentById(id)
        .then(document => {
            //if there is no document, the id is not valide
            if (!document) {
                res.status(404).json({ message: "invalid document Id" })
            }
            // no need to add a else, we already check the id validity
            dbModelsRow.addRow(row, id)
                .then(row => {
                    if (row) {
                        res.status(200).json(row)
                    }
                })
                .catch(error => {
                    res.status(500).json({ message: "impossible d'effectuer l'opération", error })
                })
        })

        .catch(error => {
            res.status(500).json({ message: "impossible d'effectuer l'opération", error })
        })
})

// get all row
router.get('/:id/row', (req, res) => {
    const { id } = req.params;

    //check the document id
    dbModelsDocument.findDocumentById(id)
        .then(document => {
            //if there is no document, the id is not valide
            if (!document) {
                res.status(404).json({ message: "invalid document Id" })
            }
            dbModelsRow.findAllRow(id)
                .then(row => {
                    if (row) {
                        res.status(200).json(row)
                    }
                })
                .catch(error => {
                    res.status(500).json({ message: "impossible d'effectuer l'opération", error })
                })
        })
        .catch(error => {
            res.status(500).json({ message: "impossible d'effectuer l'opération", error })
        })
})

//// get one row by id
router.get('/:id?/row/:rowid?', (req, res) => {
    const { id } = req.params;
    const { rowid } = req.params;
    console.log("rowid", rowid, "id", id)

    //check the document id
    dbModelsDocument.findDocumentById(id)
        .then(document => {
            //if there is no document, the id is not valide
            if (!document) {
                res.status(404).json({ message: "invalid document Id" })
            }
            dbModelsRow.findRowById(rowid)
                .then(row => {
                    if (row) {
                        res.status(200).json(row)
                    }
                    else{
                        res.status(404).json({ message: "invalid row Id" })
                    }
                })
                .catch(error => {
                    res.status(500).json({ message: "impossible d'effectuer l'opération", error })
                })
        })
        .catch(error => {
            res.status(500).json({ message: "impossible d'effectuer l'opération", error })
        })
})

//patch row
router.patch('/:id?/row/:rowid?', (req, res) => {
    const { id } = req.params;
    const { rowid } = req.params;
    const changes = req.body

    //check the document id
    dbModelsDocument.findDocumentById(id)
        .then(document => {
            //if there is no document, the id is not valide
            if (!document) {
                res.status(404).json({ message: "invalid document Id" })
            }
            dbModelsRow.updateRow(rowid, changes)
                .then(row => {
                    if (row) {
                        res.status(200).json(row)
                    }
                })
                .catch(error => {
                    res.status(500).json({ message: "impossible d'effectuer l'opération", error })
                })
        })
        .catch(error => {
            res.status(500).json({ message: "impossible d'effectuer l'opération", error })
        })
})

// delete row
router.delete('/:id?/row/:rowid?', (req, res) => {
    const { id } = req.params;
    const { rowid } = req.params;

    //check the document id
    dbModelsDocument.findDocumentById(id)
        .then(document => {
            //if there is no document, the id is not valide
            if (!document) {
                res.status(404).json({ message: "invalid document Id" })
            }
            dbModelsRow.removeRow(rowid)
                .then(count => {
                    if (count > 0) [
                        res.status(200).json({ message: "suppression effectuée" })
                    ]
                    else {
                        res.status(404).json({ message: "aucune row touvée avec cet id" })
                    }
                })
                .catch(error => {
                    res.status(500).json({ message: "impossible d'effectuer l'opération", error })
                })
        })
        .catch(error => {
            res.status(500).json({ message: "impossible d'effectuer l'opération", error })
        })
})

//patch doc
router.patch('/:id', (req, res) => {
    const { id } = req.params;
    const changes = req.body
    dbModelsDocument.updateDocument(id, changes)
        .then(document => {
            if (document) {
                res.status(200).json(document)
            }
            else {
                res.status(404).json({ message: "aucun document trouvé avec cet id" })
            }
        })
        .catch(error => {
            res.status(500).json({ message: "impossible d'effectuer l'opération", error })
        })
})

//delete doc
router.delete('/:id', (req, res) => {
    const { id } = req.params
    dbModelsDocument.removeDocument(id)
        .then(count => {
            if (count > 0) {
                res.status(200).json({ message: `document avec l'id ${id} supprimé` })
            }
            else {
                res.status(404).json({ message: "aucun doc trouvé avec cet id" })
            }
        })
        .catch(error => {
            res.status(500).json({ message: "impossible d'effectuer l'opération", error })
        })
})

module.exports = router
