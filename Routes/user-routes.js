const express = require('express')
const dbModels = require('../models/user-models');

const router = express.Router()


router.post('/', (req, res) => {
    dbModels.addUser(req.body)
        .then(user => {
            res.status(200).json(user)
        })
        .catch(error => {
            res.status(500).json({message: "impossible d'effectuer l'opération", error})
        })
})
router.get('/', (req, res) => {
    dbModels.findAllUser()
        .then(user => {
            res.status(200).json(user)
        })
        .catch(error => {
            res.status(500).json({message: "impossible d'effectuer l'opération", error})
        })
})

router.get('/:id', (req, res) => {
    const { id } = req.params;
    dbModels.findUserById(id)
        .then(user => {
            if (user) {
                res.status(200).json(user)
            }
            else {
                res.status(404).json({ message: "cet id n'a pas été trouvé dans la base user" })
            }
        })
        .catch(error => {
            res.status(500).json({message: "impossible d'effectuer l'opération", error})
        })
})

router.delete('/:id', (req, res) => {
    const { id } = req.params;
    dbModels.removeUser(id)
        //function .del() returns a number of deletion we name it count and check if it's > to zero
        .then(count => {
            if (count > 0) {
                res.status(200).json({ message: "suppression effectuée" })
            }
            else {
                res.status(404).json({ message: "aucun user trouvé avec cet id" })
            }
        })
        .catch(error => {
            res.status(500).json({message: "impossible d'effectuer l'opération", error})
        })
})

router.patch('/:id', (req, res) => {
    const { id } = req.params;
    const changes = req.body
    dbModels.updateUser(id, changes)
        .then(user => {
            if (user) {
                res.status(200).json(user)
            }
            else {
                res.status(404).json({ message: "aucun user avec cet id" })
            }
        })
        .catch(error => {
            res.status(500).json({message: "impossible d'effectuer l'opération", error})
        })
})

module.exports = router