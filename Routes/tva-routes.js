const express = require('express')
const dbModels = require('../models/tva-models');

const router = express.Router()

router.post('/', (req, res) => {
    dbModels.addTva(req.body)
        .then(tva => {
            res.status(200).json(tva)
        })
        .catch(error => {
            res.status(500).json({ message: "impossible d'effectuer l'opération", error })
        })
})

router.get('/', (req, res) => {
    dbModels.findAllTva()
        .then(tva => {
            res.status(200).json(tva)
        })
        .catch(error => {
            res.status(500).json({ message: "impossible d'effectuer l'opération", error })
        })
})

router.get('/:id', (req, res) => {
    const { id } = req.params;
    dbModels.findTvaById(id)
        .then(tva => {
            if (tva) {
                res.status(200).json(tva)
            }
            else {
                res.status(404).json({ message: "aucune tva trouvée avec cet id" })
            }
        })
        .catch(error => {
            res.status(500).json({ message: "impossible d'effectuer l'opération" })
        })
})

router.delete('/:id', (req,res)=> {
    const {id} = req.params;
    dbModels.removeTva(id)
    .then(count=>{
        if(count>0){
            res.status(200).json({message: "suppression effectuée"})
        }
        else {
            res.status(404).json({message:"aucune TVA trouvée avec cet id"})
        }
    })
    .catch(error =>{
        res.status(500).json({message: "impossible d'effectuer l'opération", error})
    })
})

router.patch('/:id', (req, res)=>{
    const {id} = req.params;
    const changes = req.body
    dbModels.updateTva(id,changes)
    .then(tva => {
        if (tva){
            res.status(200).json(tva)
        }
        else {
            res.status(404).json({message:"aucune TVA trouvée avec cet id"})
        }
    })
    .catch(error=> {
        res.status(500).json({message: "impossible d'effectuer l'opération", error}) 
    })
})

module.exports = router