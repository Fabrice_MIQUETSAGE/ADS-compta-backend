
exports.up = async function(knex) {
    await knex.schema
        .createTable('client', table => {
            table.increments('id').primary()
            table.string('company').notNullable()
            table.string('contactName').notNullable()
            table.string('email').unique().notNullable()
            table.integer('phoneNumber').notNullable()
            table.string('imgLogo')
            table.string('street')
            table.integer('zipCode')
            table.string('city')
            table.string('country')
        }) 
};

exports.down = function (knex) {
    return knex.schema.dropTableIfExists('client')
};

