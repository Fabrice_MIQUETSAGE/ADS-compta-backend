exports.up = function(knex, Promise) {
    return knex.schema.alterTable('client', function(t) {
      t.string('phoneNumber').alter();
      t.string('zipCode').alter();      
    });
  };
  
  exports.down = function(knex, Promise) {
    return knex.schema.alterTable('client', function(t) {
        t.integer('phoneNumber').alter();
        t.integer('zipCode').alter();         
    });
  };