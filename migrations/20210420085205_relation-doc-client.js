
exports.up = async function (knex) {
    await knex.schema.table('document', table => {
        //foreign key to user
        table
            .integer('client_id')
            .references('id')
            .inTable('client')
            .onDelete('CASCADE')
    })

};

exports.down = function (knex) {
    return knex.schema.table('document', table => { table.dropColumn('client_id') })
};
