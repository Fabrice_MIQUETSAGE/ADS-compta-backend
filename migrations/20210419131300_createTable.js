
exports.up = async function (knex) {

    await knex.schema
        .createTable('user', table => {
            table.increments('id').primary()
            table.string('name')
                .notNullable()
            table.integer('pinNumber')
                .notNullable()
            table.string('email')
                .unique()
                .notNullable()
        })
        await knex.schema.createTable('docType', table => {
            table.increments('id').primary()
            table.timestamps(true, true)
            table.string('type')
        })
        await knex.schema
        .createTable('document', table => {
            table.increments('id').primary()
            table.timestamps(true, true)
            table.float('totalHT').defaultTo(0)
            table.float('totalTva').defaultTo(0)
            table.float('totalDiscount').defaultTo(0)
            table.float('totalTTC').defaultTo(0)
            //foreign key to user
            table
                .integer('user_id')
                .references('id')
                .inTable('user')
                .onDelete('CASCADE')
            //foreign key to docType
            table.integer('docType_id')
                .references('id')
                .inTable('docType')
                .unsigned()
                .onDelete('CASCADE')
        })
        await knex.schema.createTable('tva', table => {
            table.increments('id').primary()
            table.string('label')
            table.float('taux').defaultTo(0)
        })

        await knex.schema.createTable('row', table => {
            table.increments('id').primary()
            table.string('apiProductId')
            table.string('apiProductName')
            table.float('apiProduct_unitPrice').defaultTo(0)
            table.integer('quantity').defaultTo(0)
            table.float('subTotalPrice').defaultTo(0)
            table.float('discount').defaultTo(0)
            table.float('totalPriceHT').defaultTo(0)
            table.float('totalPriceTTC').defaultTo(0)
            table.integer('position').unsigned()
            table.text('comments')
            // foreign key to tva table
            table.integer('tva_id')
                .references('id')
                .inTable('tva')
                .unsigned() 
                .onDelete('CASCADE')
            // foreign key to document table
            table.integer('document_id')
                .references('id')
                .inTable('document')
                .unsigned() 
                .onDelete('CASCADE')    
        })
};

exports.down = function (knex) {
    return knex.schema.dropTableIfExists('user').dropTableIfExists('row').dropTableIfExists('tva').dropTableIfExists('document').dropTableIfExists('docType')
};
