const express = require('express')

const cors = require('cors')
//const jwt = require('jsonwebtoken')
const userRouter = require('../Routes/user-routes')
const tvaRouter = require('../Routes/tva-routes')
const documentRouter = require('../Routes/document-routes')
const rowRouter = require('../Routes/row-routes')
const docTypeRouter = require ('../Routes/docType-routes')
const clientRouter = require ('../Routes/client-routes')
const loginRouter = require ('../Routes/login-routes')
const passport = require("passport")
const LocalStrategy = require("passport-local")
const passportJWT = require("passport-jwt")
const JWTStrategy = passportJWT.Strategy
const db = require('../dbConfig')
const restricted = require('../middleware/restricted-middleware')


const server = express();
server.use(cacheControl({
    noCache: true
  }));
server.use(passport.initialize())


 
passport.use(new LocalStrategy({ usernameField: 'email', }, async (email, password, done) => {
    try {
        // Lookup the user 
        const user = await db("user").first().where({email})
        // If the user does not exist
        if (!user) {
            return done(null, false);
        }
        if (user){
            
            if(email === user.email && password === user.pinNumber) {
            return done(null, user)
        }
        else {
            return done(null, false)
          } 
        }
    } catch (err) {
        passLog.error(`Login Error: ${err}`);
    }

}));

passport.use(new JWTStrategy({
    jwtFromRequest : passportJWT.ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: process.env.JWT_SECRET
}, (jwt_payload, done)=> {
    if(user.id === jwt_payload.user._id){
        return done(null, user)}
        else {
            return done(null, false, {
                message: "le token ne correspond pas"
           })
  }
}))

// 

server.use(express.json())
const corsOptions = {
    origin: 'http://localhost:3000',
    optionsSuccessStatus: 200 // For legacy browser support
}

server.use(cors(corsOptions));
server.get('/', (req, res) => {
    res.json({ message: 'Hello World!' })
});
server.use('/api/user', restricted, userRouter)
server.use('/api/tva', restricted, tvaRouter)
server.use('/api/document',restricted, documentRouter)
server.use('/api/row', restricted,rowRouter)
server.use('/api/docType', restricted, docTypeRouter)
server.use('/api/client', restricted, clientRouter)
server.use('/api/login', loginRouter)

module.exports = server