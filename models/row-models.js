const db= require('../dbConfig')

//CRUD function
module.exports = {
    addRow,
    findAllRow,
    findRowById,
    removeRow,
    updateRow
}

//add a Row
async function addRow(row, document_id) {
    return await db('row')
        .where({ document_id: document_id })
        .insert(row, ['id'])
}


function findAllRow(document_id) {
    //start with parent db
    return db('row').select('row.*')
        // join ("child table", "parent primary key" "child foreign key")
        //.join("row", "document.id", "row.document_id")
        //.select()
        .where({document_id})
}

//find a specific row
async function findRowById(id) {
    const row = await db('row').select('row.*').first().where({ id: id })
    const tvaID = row.tva_id 
    const tva = await db("tva").where({id: tvaID }).select('taux as tva_taux', 'label as tva_label', 'id as tva_id')
    return {row, tva}
       
       
}

//remove row
function removeRow(id) {
    return db('row')
        .where({ id: id })
        .del()
}

//update row
function updateRow(id, changes) {
    return db('row')
        .where({ id: id })
        .update(changes)
        .then(() => {
            return findRowById(id)
        })
}