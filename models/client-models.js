const db = require('../dbConfig')

//CRUD function
module.exports = {
    addClient,
    findAllClient,
    findClientById,
    updateClient,
    removeClient
}

//add a client
async function addClient(client) {
    return await db('client').insert(client, ['id'])
}

// find a specific client by id
async function findClientById(id) {
const client = await db("client").first().where({id})
const doc = await db("document").select('document.*')
.where({ client_id: id })
.innerJoin("docType", "docType_id", "docType.id").select('type as document_type') 
.innerJoin("user", "user_id", "user.id").select('name as user_name', 'user_id as userID') 
.innerJoin("client", "client_id", "client.id").select('company as client_company', 'client_id as clientID') 
return [client, doc]
}

//find all client
function findAllClient() {
    return db('client')
}

//update client
function updateClient(id, changes){
    return db('client')
    .where({id})
    .update(changes)
    .then(()=> {
        return findClientById(id)
    })
}

//remove client by his id
function removeClient(id) {
    return db('client')
        .where({ id: id })
        .del()
}