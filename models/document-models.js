// before dbConfig.js wee needed this code
// knex queries
// const knex = require('knex')
// //need to know how to interact with ou db
// const config = require('../knexfile')
// const db = knex(config.development)

const db = require('../dbConfig')

//CRUD function
module.exports = {
    addDocument,
    findAllDocument,
    findDocumentById,
    removeDocument,
    updateDocument
}

//add Document
async function addDocument(document) {
    return await db('document').insert(document, ['id'])
}

//findAll
async function findAllDocument() {
    const doc = db("document").select('document.*')
    .innerJoin("docType", "docType_id", "docType.id").select('type as document_type') 
    .innerJoin("client", "client_id", "client.id").select('company as client_company', 'client_id as clientID')
    .innerJoin("user", "user_id", "user.id").select('name as user_name', 'user_id as userID')
   return doc
}


//find a specific document
async function findDocumentById(id) {
    const doc = await db("document").first().where({id})
    const userID= doc.user_id
    const clientID = doc.client_id
    const user = await db("user").where({id: userID })
const client = await db("client").where({id:clientID})
    const row = await db("row").select("row.*").where({ document_id: id })
    .join("tva", "tva_id", "tva.id") 
    return [doc, user, row, client]
}

// update document
function updateDocument(id, changes) {
    return db('document')
        .where({ id: id })
        .update(changes)
        .then(() => {
            return findDocumentById(id)
        })
}

//remove document
function removeDocument(id) {
    return db('document')
        .where({ id: id })
        .del()
}

