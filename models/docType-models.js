const db = require('../dbConfig')

//CRUD function
module.exports = {
    addDocType,
    findAllDocType,
    findDocTypeById,
    removeDocType
}

//add a docType
async function addDocType(docType) {
   return await db('docType').insert(docType, ['id'])
}

// find a specific docType by id
function findDocTypeById(id) {
    return db('docType')
        .where({ id: id })
        .first()
}

//find all docType
function findAllDocType() {
    return db('docType')
}

//remove docType
function removeDocType(id){
    return db('docType')
    .where({id})
    .del()
}

