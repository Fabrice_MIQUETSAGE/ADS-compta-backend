const db= require('../dbConfig')

//CRUD function

module.exports = {
    addUser,
    findAllUser,
    findUserById,
    removeUser,
    updateUser, 
    findUserByEmail,
};

//add a user
async function addUser(user) {
    return await db('user').insert(user, ['id'])
}

//find allUser
function findAllUser() {
    return db('user')
}
//find a user by email
async function findUserByEmail(email) {
    const user = await db("user").first().where({email})
    return [user]
}


//find a specific user
async function findUserById(id) {
    const user = await db("user").first().where({id})
    const doc = await db("document")
    .where({ user_id: id })
    .join("docType", "docType_id", "docType.id")   
;
    return [user, doc]
}

//remove user by his id
function removeUser(id) {
    return db('user')
        .where({ id: id })
        .del()
}

//upDate user put or patch
//put is a completeley update
//patch is a corrective
function updateUser(id, changes) {
    return db('user')
        .where({ id })
        .update(changes)
        //add the function findUserById to return the user updated
        .then(() => {
            return findUserById(id)
        });
}

