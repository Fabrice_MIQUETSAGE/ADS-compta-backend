const db= require('../dbConfig')

//CRUD function
module.exports = {
    addTva, 
    findAllTva, 
    findTvaById, 
    removeTva,
    updateTva
}

//add TVA
async function addTva(tva) {
    return await db('tva').insert(tva, ['id'])
}

// findAll
function findAllTva(){
    return db('tva')
}

//find a specific TVA
function findTvaById(id){
    return db('tva')
    .where({id:id})
    .first()
}

//remove Tva
function removeTva(id){
    return db('tva')
    .where({id:id})
    .del()
}

// update
function updateTva(id, changes){
    return db('tva')
    .where({id:id})
    .update(changes)
    .then(()=>{
        return findTvaById(id)
    });
}