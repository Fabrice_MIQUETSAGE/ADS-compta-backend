module.exports = {

  development: {
    client: 'sqlite3',
    // only with sqlite
    useNullAsDefault: true,
    connection: {
      filename: './data/app2.db3'
    },
    pool: {
      afterCreate: (conn, done) => {
        conn.run("PRAGMA foreign_keys = ON", done);
      }
    }
  },

  production: {
    client: 'pg',
    connection: process.env.DATABASE_URL, 
    pool: {
     min:2,
     max:10, 
    },
    ssl: {
      "require": true,
      "rejectUnauthorized": false
    },

  },
  migrations: {
    directory: './migrations'
  }
 };
